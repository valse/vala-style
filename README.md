# Vala style checker

This little script will check that your Vala code respects the following conventions :

- There is always a space before a `(`, a `[` or a `{`
- You put a space before and after all math symbols and numbers
- You use "One True Brace Style" (braces for opening a block at the end of the line)
- You use UPPER_SNAKE_CASE for constants
- You don't use `as`
- You don't use `GLib` when not necessary

## Integration with Gitlab CI

You can use this `gitlab-ci.yml` to test your code.

```yml
image: fedora:latest

before_script:
    - dnf install ruby wget -y

style:
  script:
    - wget https://framagit.org/valse/vala-style/raw/master/style.rb
    - ruby style.rb
```

Then you can go in your repo settings and set *Test Coverage Parsing* to `\d+.\d+`.
Now you'll have the percentage of correct code in the Coverage column of your builds.

![Coverage](https://bat41.gitlab.io/images/coverage.PNG)
